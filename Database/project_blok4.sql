-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 29 jun 2017 om 23:56
-- Serverversie: 10.1.21-MariaDB
-- PHP-versie: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_blok4`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `notes`
--

CREATE TABLE `notes` (
  `note_id` int(10) UNSIGNED NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `visible_to_student` tinyint(1) UNSIGNED DEFAULT '0',
  `visible_to_teachers` tinyint(1) DEFAULT '0',
  `title` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `last_edited` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `notes`
--

INSERT INTO `notes` (`note_id`, `author_id`, `student_id`, `creation_date`, `visible_to_student`, `visible_to_teachers`, `title`, `description`, `last_edited`) VALUES
(2, 1, 2, '2017-06-04 22:00:00', 1, 1, 'Black', 'XD', NULL),
(8, 2, 2, '2017-06-28 16:41:22', 0, 0, 'asd', 'asd', '2017-06-28 16:41:22'),
(9, 2, 2, '2017-06-28 16:45:08', 0, NULL, 'asdasd243', 'asdsad23454', NULL),
(10, 1, 3, '2017-06-28 20:53:20', 0, NULL, 'asd3445', 'sadasdzxc4yytghm', NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ranks`
--

CREATE TABLE `ranks` (
  `rank_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `ranks`
--

INSERT INTO `ranks` (`rank_id`, `name`) VALUES
(3, 'admin'),
(1, 'student'),
(2, 'teacher');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `student_mentor`
--

CREATE TABLE `student_mentor` (
  `student_id` int(10) UNSIGNED NOT NULL,
  `teacher_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `student_mentor`
--

INSERT INTO `student_mentor` (`student_id`, `teacher_id`) VALUES
(4, 2),
(3, 6);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `rank_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `date_of_birth` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `rank_id`, `first_name`, `last_name`, `date_of_birth`) VALUES
(1, 'Toxyc', 'lol123', 3, 'Thijs', 'de Jonge', '1999-04-29'),
(2, 'docent', 'lol123', 2, 'Piet', 'xD', '1956-02-28'),
(3, 'student', 'lol123', 1, 'Gert', 'xD', '1956-02-28'),
(4, 'student2', 'lol123', 1, 'Hans', 'van Leeuwen', '2006-09-30'),
(5, 'student3', 'lol123', 1, 'Studentje', 'no Mentor', '1999-04-29'),
(6, 'docent2', 'lol123', 2, 'Docent', 'Meneer', '2017-06-13');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`note_id`),
  ADD KEY `user_id to student_idx` (`student_id`),
  ADD KEY `user_id to author` (`author_id`);

--
-- Indexen voor tabel `ranks`
--
ALTER TABLE `ranks`
  ADD PRIMARY KEY (`rank_id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexen voor tabel `student_mentor`
--
ALTER TABLE `student_mentor`
  ADD PRIMARY KEY (`student_id`),
  ADD KEY `user_id_idx` (`teacher_id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD KEY `rank_id_idx` (`rank_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `notes`
--
ALTER TABLE `notes`
  MODIFY `note_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT voor een tabel `ranks`
--
ALTER TABLE `ranks`
  MODIFY `rank_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `user_id to author` FOREIGN KEY (`author_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_id to student` FOREIGN KEY (`student_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `student_mentor`
--
ALTER TABLE `student_mentor`
  ADD CONSTRAINT `user_id to student2` FOREIGN KEY (`student_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_id to teacher` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Beperkingen voor tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `rank_id` FOREIGN KEY (`rank_id`) REFERENCES `ranks` (`rank_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
