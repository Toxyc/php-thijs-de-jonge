<?php

  class Notes extends Users {

    protected $pdo;

    public function __construct($pdo) {
        $this->pdo = $pdo;
    }

      public function getNotesFromStudent($student_id = 0) {
          $teacher_id = $_SESSION['user_id'];
          if ($this->checkRank() == 'Student') {
              $user_id = $_SESSION['user_id'];
              $stmt = $this->pdo->prepare("SELECT * FROM notes WHERE student_id = $user_id && visible_to_student = 1 ORDER BY creation_date DESC");
          }
          else if ($this->checkRank() == 'Admin' || ($this->checkRank() == 'Teacher' && $this->isMentor($student_id, $teacher_id))) {
              $stmt = $this->pdo->prepare("SELECT * FROM notes WHERE student_id =:student_id ORDER BY creation_date DESC");
              $stmt->bindValue(':student_id', $student_id, PDO::PARAM_INT);
          }
          else if($this->checkRank() == 'Teacher') {
              $stmt = $this->pdo->prepare("SELECT * FROM notes WHERE student_id =:student_id && (author_id =:teacher_id || visible_to_teachers = 1) ORDER BY creation_date DESC");
              $stmt->bindValue(':student_id', $student_id, PDO::PARAM_INT);
              $stmt->bindValue(':teacher_id', $teacher_id, PDO::PARAM_INT);
          }
          $stmt->execute();
          return $stmt->fetchAll(PDO::FETCH_ASSOC);
      }

    public function insert() {
        $errors = [];
        $author_id = $_SESSION['user_id'];
        $visible_to_student = filter_input(INPUT_POST, 'visible_to_student');
        $visible_to_teachers = filter_input(INPUT_POST, 'visible_to_teachers');
        $title = $this->validatedTitle($errors);
        $description = $this->validatedDescription($errors);
        $student_id = filter_input(INPUT_POST, 'student_id');
        $query =
       "INSERT
        INTO notes (author_id, student_id, visible_to_student, visible_to_teachers, title, description)
        VALUES (:author_id, :student_id, :visible_to_student, :visible_to_teachers, :title, :description)";
      $stmt = $this->pdo->prepare($query);
      $stmt->bindValue(':author_id', $author_id, PDO::PARAM_INT);
      $stmt->bindValue(':student_id', $student_id, PDO::PARAM_INT);
      $stmt->bindValue(':visible_to_student', $visible_to_student, PDO::PARAM_BOOL);
      $stmt->bindValue(':visible_to_teachers', $visible_to_teachers, PDO::PARAM_BOOL);
      $stmt->bindValue(':description', $description, PDO::PARAM_STR);
      $stmt->bindValue(':title', $title, PDO::PARAM_STR);
      $ok = $stmt->execute();
      if (!$ok) {
        $info = $stmt->errorInfo();
        die($info[2]);
      }
    }

      function delete() {
          $note_id = filter_input(INPUT_GET, 'id');
          $query = 'DELETE FROM notes WHERE note_id=:note_id';
          $stmt = $this->pdo->prepare($query);
          $stmt->bindValue(':note_id', $note_id, PDO::PARAM_INT);
          $ok = $stmt->execute();
          if (!$ok) {
              $info = $stmt->errorInfo();
              die($info[2]);
          }
          return $stmt->rowCount();
      }

    public function validatedTitle (&$errors, $except_id = NULL) {
        $title = filter_input(INPUT_POST, 'title');
        if (empty ($title)) {
            $errors['title'] = 'Title is missing';
        } else {
            $title = trim($title);
            if ($title == '') {
                $errors['title'] = 'Title may not be empty';
            } elseif (strlen($title) > 255) {
                $errors['title'] = 'Title may not exceed 255 characters.';
            }
          }
        return $title;
    }

    public function validatedDescription (&$errors, $except_id = NULL)
    {
        $description = filter_input(INPUT_POST, 'description');
        if (empty ($description)) {
            $errors['description'] = 'Description is missing';
        } else {
            $description = trim($description);
            if ($description == '') {
                $errors['description'] = 'Description may not be empty';
            } elseif (strlen($description) > 255) {
                $errors['description'] = 'Description may not exceed 255 characters.';
            }
        }
        return $description;
    }

      public function getStudentFullName($student_id) {
          $stmt = $this->pdo->prepare("SELECT * FROM users WHERE user_id = $student_id");
          $stmt->execute();
          $rows = $stmt->fetch(PDO::FETCH_ASSOC);
          $fullname = $rows['first_name']." ".$rows['last_name'];
          return $fullname;
      }

      public function getAuthorName ($teacher_id) {
          $stmt = $this->pdo->prepare("SELECT u.* FROM notes n INNER JOIN users u ON n.author_id = u.user_id WHERE author_id = $teacher_id");
          $stmt->execute();
          $rows = $stmt->fetch(PDO::FETCH_ASSOC);
          $fullname = $rows['first_name']." ".$rows['last_name'];
          return $fullname;
      }

      public function getNoteByID ($note_id) {
          $stmt = $this->pdo->prepare("SELECT * FROM notes WHERE note_id = :note_id");
          $stmt->bindValue(':note_id', $note_id, PDO::PARAM_INT);
          $stmt->execute();
          $row = $stmt->fetch(PDO::FETCH_ASSOC);
          return $row;
      }
  }
