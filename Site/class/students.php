<?php

  class Students extends Users {

    public function getMentorStudents ($teacher_id) {
        $stmt = $this->pdo->prepare("SELECT u.* FROM student_mentor sm INNER JOIN users u 
                                   ON sm.student_id = u.user_id 
                                   WHERE sm.teacher_id = $teacher_id");
        $stmt->bindValue(':teacher_id', $teacher_id, PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function editMentor () {
            $mentor_id = filter_input(INPUT_POST, 'mentor_id');
            $student_id = filter_input(INPUT_POST, 'student_id');
            $query =
                "UPDATE student_mentor
                 SET teacher_id = :mentor_id
                 WHERE student_id = :student_id";
            $stmt = $this->pdo->prepare($query);
            $stmt->bindValue(':mentor_id', $mentor_id, PDO::PARAM_INT);
            $stmt->bindValue(':student_id', $student_id, PDO::PARAM_INT);
            $ok = $stmt->execute();
            if (!$ok) {
                $info = $stmt->errorInfo();
                die($info[2]);
            }
    }

  }
