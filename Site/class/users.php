<?php

class Users {

    protected $pdo;

    public function __construct($pdo) {
        $this->pdo = $pdo;
    }

    private function isLoggedin() {
      if(isset($_SESSION['user_id'])) {
         return true;
      }
   }

   public function redirect($s, $url) {
     header("Refresh: ".$s."; URL=".$url);
   }

   public function logout() {
        session_destroy();
        unset($_SESSION['user_id']);
        $this->redirect(0, 'index.php');
        return true;
   }

 public function login($username, $password) {
       try {
          $stmt = $this->pdo->prepare("SELECT * FROM users WHERE username=:username");
          $stmt->bindValue(':username', $username, PDO::PARAM_STR);
          $stmt->execute();
          $row = $stmt->fetch(PDO::FETCH_ASSOC);
          if($stmt->rowCount() > 0) {
             if($password == $row['password']) {
                $_SESSION['user_id'] = $row['user_id'];
                if ($this->checkRank() == 'Teacher') { $this->redirect(0, '../Teachers/index.php'); }
                else if ($this->checkRank() == 'Admin') { $this->redirect(0, '../Admin/index.php'); }
                else { $this->redirect(0, '../Students/index.php'); }
                return true;
             }
             else {
                return false;
             }
          }
       }
       catch(PDOException $e) {
           echo $e->getMessage();
       }
     }

     public function getFullNameFromId($id) {
       $stmt = $this->pdo->prepare("SELECT first_name, last_name FROM users WHERE user_id='$id'");
       $stmt->execute();
       $row = $stmt->fetch(PDO::FETCH_ASSOC);
       return $row;
     }

     public function checkForLoginPost() {
       if (isset($_POST['logoutButton'])) {
         $this->logout();
       }

       if (isset($_POST['loginButton'])) {
         $username = $_POST['username'];
         $password = $_POST['password'];
         $loggedIn = $this->login($username, $password);

         if (!$loggedIn) {
           echo "Invalid credentials, please try again.";
          }
       }
     }

     public function checkForSession() {
       if (isset($_SESSION['user_id'])) {
         $fullname = $this->getFullNameFromId($_SESSION['user_id']);
         echo "Welcome, ".$fullname['first_name']." ".$fullname['last_name'];
         echo "<br><button type='submit' name='logoutButton'>Logout</button>";
         return true;
       }
       else {
        echo "<input type='text' name='username' placeholder='Cooleboy123'>
          <input type='password' name='password'>
          <button type='submit' name='loginButton'>Login</button>";
          return false;
        }
     }

     public function checkRank() {
       if(isset($_SESSION['user_id'])) {
         $user_id = $_SESSION['user_id'];
       }
       $stmt = $this->pdo->prepare("SELECT rank_id FROM users WHERE user_id='$user_id'");
       $stmt->execute();
       $row = $stmt->fetch(PDO::FETCH_ASSOC);
       if($stmt->rowCount() > 0) {
         switch ($row['rank_id']) {
          case 1:
            $_SESSION['rank'] = 'Student';
            break;
          case 2:
            $_SESSION['rank'] = 'Teacher';
            break;
          case 3:
            $_SESSION['rank'] = 'Admin';
            break;
          }
          return $_SESSION['rank'];
        }
        else {
          return false;
        }
      }

      public function getStudents($order = 'id') {
        $stmt = $this->pdo->prepare("SELECT * FROM users WHERE rank_id = 1 ORDER BY $order");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
      }

      public function getMentor($user_id) {
        $stmt = $this->pdo->prepare(
          "SELECT *
          FROM student_mentor sm
          INNER JOIN users u ON u.user_id = sm.teacher_id
          WHERE sm.student_id =$user_id"
        );
        $stmt->execute();
        if($stmt->rowCount() > 0) {
          $row = $stmt->fetch(PDO::FETCH_ASSOC);
          $fullname = $row['first_name']." ".$row['last_name'];
          return $fullname;
        }
        else {
          return "<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
        }
      }

        // Checks if the teacher in question is the mentor of the student in question.
      public function isMentor($student_id, $teacher_id) {
        $stmt = $this->pdo->prepare(
          "SELECT *
          FROM student_mentor sm
          INNER JOIN users u ON u.user_id = sm.teacher_id
          WHERE sm.student_id = $student_id && sm.teacher_id = $teacher_id"
        );
        $ok = $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$ok) {
          echo "SQL Error.";
        }
      }

      // Checks if the user is a mentor in general
    public function isAMentor($teacher_id) {
        $stmt = $this->pdo->prepare(
            "SELECT *
          FROM student_mentor sm
          WHERE sm.teacher_id = $teacher_id"
        );
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public function getTeachers () {
        $stmt = $this->pdo->prepare("SELECT * FROM users WHERE rank_id = 2");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

}
