<?php

/**
 * @author Jeroen Van den Brink
 * @copyright 2017
 * 
 * De functies in deze include vallen uiteen in twee soorten: 
 * functies voor de request en functies voor de response.
 * Beter is het dus om dit bestand te splitsen in twee include-bestanden.
 */

/**
 * redirectWithMessage()
 * 
 * Ga naar een andere pagina ($redirect) en stop boodschap ($message) in de sessie
 * 
 * @param string $redirect
 * @param string $message
 * @return void
 */
function redirectWithMessage($redirect, $message) {
    $_SESSION['message'] = $message;
    header('location: '.$redirect);
    die;
}

/**
 * checkForErrors()
 * 
 * Controleer of er validatiefouten bestaan.
 * Zo ja:
 * - stop algemene foutboodschap ($message) in de sessie
 * - stop de veldfouten ($errors) in de sessie
 * - stop de oude input ($input) in de sessie
 * - redirect naar $redirect
 * 
 * @param string $message
 * @param array $errors
 * @param array $input
 * @param string $redirect
 * @return void
 */
function checkForErrors($message, $errors, $input, $redirect)
{
    if (count($errors) > 0)
    {
        $_SESSION['errors'] = $errors;
        $_SESSION['input'] = $input;
        redirectWithMessage($redirect, $message);
    }
}

/**
 * checkRequestMethod()
 *
 * check of pagina/script correct wordt aangeroepen; 
 * redirect naar $redirect indien incorrect
 *  
 * @param string $method
 * @param string $redirect
 * @return void
 */
function checkRequestMethod($method, $redirect) {
    if ($_SERVER['REQUEST_METHOD'] != $method) {
        redirectWithMessage($redirect, 'Ongeldige aanroep van ' . basename(__FILE__) . '; moet een ' . $method . ' request zijn');
    }
}

/**
 * getRequestId()
 * 
 * Check of er een id in de querystring of post is meegegeven en retourneer deze.
 * Redirect naar $redirect indien er geen id is.
 * 
 * @param integer $method (1 van de constanten INPUT_GET of INPUT_POST)
 * @param string $redirect
 * @return integer
 */
function getRequestId($method, $redirect) {
    $id = filter_input($method, 'id', FILTER_VALIDATE_INT);
    if (empty ($id)) {
        redirectWithMessage($redirect, 'Ongeldige aanroep van ' . basename(__FILE__) . '; (geldige) id ontbreekt');
    }
    return $id;
}
