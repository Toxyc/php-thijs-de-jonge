<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Notes</title>
    <link rel="stylesheet" href="../../include/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>

<?php

require_once('../../include/autoload.php');
require_once('../../include/session.php');
require_once('../../include/common.php');


$pdo = new MyPDO();
$user = new Users($pdo);

$user->checkForLoginPost();
?>

<form class="form" action="index.php" method="POST">
  <?php $loggedIn = $user->checkForSession() ?>
</form>

<?php
  if ($loggedIn) { $rank = $user->checkRank(); }
  if (isset($rank)) {
    if ($rank == 'Admin') {
      if (isset($_GET['order'])) { $order = $_GET['order']; }
      else { $order = 'user_id'; }
      $students = $user->getStudents($order);
    } else { echo "You have to be logged in as an Admin to view this page."; }
  }
  if (isset($students)) {
  ?>
  <table>
    <thead>
      <tr>
        <a href="?order=user_id"><th>ID</th></a>
        <a href="?order=first_name"><th>Full name</th></a>
        <a href="?order=date_of_birth"><th>Date of birth</th></a>
        <a href="?order=username"><th>Username</th></a>
        <th>Mentor</th>
        <th>Edit</th>
      </tr>
    </thead>

    <tbody>
  <?php
    foreach($students as $student) {
      $student['mentor'] = $user->getMentor($student['user_id']);
      ?>
      <tr>
        <td class="id"><a href="../Students/index.php?id=<?= $student['user_id']?>"><i class="fa fa-id-badge" aria-hidden="true"></i></a></td>
        <td class="full_name"><?= $student['first_name']." ".$student['last_name'] ?></td>
        <td class="date_of_birth"><?= $student['date_of_birth'] ?></td>
        <td class="username"><?= $student['username'] ?></td>
        <td class="mentor"><?= $student['mentor'] ?></td>
        <td><a href="edit.php?id=<?= $student['user_id'] ?>"><i class="fa fa-wrench" aria-hidden="true"></i></a></td>
      </tr>
      <?php }
          }
          else if(isset($msg)) {
            echo $msg;
          }

     ?>
  </tbody>

  </body>
