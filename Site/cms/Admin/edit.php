<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Teachers</title>
    <link rel="stylesheet" href="../../include/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<?php

require_once('../../include/autoload.php');
require_once('../../include/session.php');
require_once('../../include/common.php');


$pdo = new MyPDO();
$note = new Notes($pdo);
$user = new Users($pdo);
$student = new Students($pdo);
$user->checkForLoginPost();
$errors = [];

?>

<form class="form" action="index.php" method="POST">
    <?php $loggedIn = $user->checkForSession() ?>
</form>

<?php
if ($loggedIn) { $rank = $user->checkRank(); }
if (isset($rank)) {
    if ($rank == 'Admin') {
        echo "You are currently on the admin edit page, you can edit mentors here. <a href='../Admin/index.php' class='btn_back'>Go back</a>";
    if (isset($_GET['id'])) {
        $teachers = $user->getTeachers();
        $fullname = $user->getFullNameFromId($_GET['id']);
    ?>

    <h1>Edit student's mentor</h1>
    <form action="edit.php" method="POST">
        <label for="student_id">
            <input type="hidden" name="student_id" value="<?= $_GET['id'] ?>">
        </label>
        <label for="student_name">Student's name
            <input type="text" name="student_name" value="<?= $fullname['first_name'] . " " . $fullname['last_name'] ?>" disabled>
        </label><br>
        <label for="id">Mentor name:
            <select name="mentor_id">
                <?php foreach ($teachers as $teacher) { ?>
                    <option value="<?= $teacher['user_id'] ?>"><?= $teacher['first_name'] ." ". $teacher['last_name'] ?></option>
                <?php } ?>
            </select>
        </label><br>
        <button type="submit" name="submit">Submit</button>
    </form>

    <?php
}    }
else {
    $msg = "You have to be logged in as an admin or teacher to view this page.";
}
    if (isset($_POST['submit'])) {
        $student->editMentor();
        $user->redirect(0, "index.php");
    }
}
if(isset($msg)) {
    echo $msg;
}