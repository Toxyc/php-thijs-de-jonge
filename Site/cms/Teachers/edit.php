<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Teachers</title>
    <link rel="stylesheet" href="../../include/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<?php

require_once('../../include/autoload.php');
require_once('../../include/session.php');
require_once('../../include/common.php');


$pdo = new MyPDO();
$note = new Notes($pdo);
$user = new Users($pdo);
$user->checkForLoginPost();
$errors = [];
if (isset($_GET['id'])) {
    $noteByID = $note->getNoteByID($_GET['id']);

?>

<form class="form" action="index.php" method="POST">
    <?php $loggedIn = $user->checkForSession() ?>
</form>

<?php
    if ($loggedIn) { $rank = $user->checkRank(); }
if (isset($rank)) {
    if ($rank == 'Admin' || ($rank == 'Teacher' && $noteByID['author_id'] == $_SESSION['user_id'])) {
        if (isset($_POST['submit'])) {
            $note->update();
            $user->redirect(0, '../Teachers/index.php');
        }
    }
}
} else {
    $msg = "You have to be logged in as an admin or teacher to view this page.";
}
?>
<h1>Edit note</h1>
<form action="edit.php" method="POST">
    <label for="note_id">
        <input type="hidden" name="note_id" value="<?= $_GET['id'] ?>">
    </label><br>
    <label for="id">Student's name
        <input type="text" value="<?= $note->getStudentFullName($noteByID['student_id']); ?>" disabled>
    </label><br>
    <label for="title">Title
        <input type="text" name="title" value="<?= $noteByID['title']; ?>">
    </label><br>
    <label for="description">Description<br>
        <textarea style="width:500px; height: 200px;" name="description"
                  value="<?= $noteByID['description']; ?>"></textarea>
    </label><br>
    <label for="permissions"><b>Permissions</b><br>
        <input type="checkbox" name="visible_to_student" <?php if ($noteByID['visible_to_student'] = 1) {
            echo "checked";
        } ?>>Visible to student<br>
        <input type="checkbox" name="visible_to_teachers" <?php if ($noteByID['visible_to_teachers'] = 1) {
            echo "checked";
        } ?>>Visible to teachers
    </label><br>
    <button type="submit" name="submit">Submit</button>
</form>

<?php
    if (isset($_POST['submit'])) {
        $note->insert();
        $user->redirect(0, "../Students/index.php?student_id=$student_id");
    } else {
        $msg = "Invalid request: Must be POST with a student ID";
    }
