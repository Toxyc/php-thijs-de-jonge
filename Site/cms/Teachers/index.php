<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Teachers</title>
    <link rel="stylesheet" href="../../include/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
<?php

require_once('../../include/autoload.php');
require_once('../../include/session.php');
require_once('../../include/common.php');


$pdo = new MyPDO();
$user = new Users($pdo);
$note = new Notes($pdo);
$student = new Students($pdo);
$user->checkForLoginPost();

?>

<form class="form" action="index.php" method="POST">
  <?php $loggedIn = $user->checkForSession() ?>
</form>

<?php
if ($loggedIn) { $rank = $user->checkRank(); }
if (isset($rank)) {
  if (isset($_GET['order'])) { $order = $_GET['order']; }
  else { $order = 'user_id'; }
  $students = $user->getStudents($order);
  if ($rank == 'Admin' || $rank == 'Teacher') { $students = $user->getStudents($order); }
}
else {
  $msg = "You have to be logged in as an admin or teacher to view this page.";
}
if (isset($students) && empty($_GET['type'])) {
?>
<table>
  <thead>
    <tr>
      <th><a href="index.php?order=user_id">ID</a></th>
      <th><a href="index.php?order=first_name">Full name</a></th>
      <th><a href="index.php?order=date_of_birth">Date of birth</a></th>
      <th><a href="index.php?order=username">Username</a></th>
      <th>Mentor</th>
      <th>Create</th>
    </tr>
  </thead>

  <tbody>
<?php
  foreach($students as $student) {
    $student['mentor'] = $user->getMentor($student['user_id']);
    ?>
    <tr>
        <td class="id"><a href="../Students/index.php?id=<?= $student['user_id']?>"><i class="fa fa-id-badge" aria-hidden="true"></i></a></td>
        <td class="full_name"><?= $student['first_name']." ".$student['last_name'] ?></td>
        <td class="date_of_birth"><?= $student['date_of_birth'] ?></td>
        <td class="username"><?= $student['username'] ?></td>
        <td class="username"><?= $student['mentor'] ?></td>
        <td class="id"><a href="../Teachers/create.php?student_id=<?= $student['user_id']?>">Create note</a></td>
    </tr>
    <?php }
        }
        if(isset($msg)) {
          echo $msg;
        }
    if (isset($_SESSION['user_id'])) {
if ($user->isAMentor($_SESSION['user_id'])) {
if (empty($_GET['type'])) {
    echo "<a href='index.php?type=mentor'>Mentorstudents list</a>";
}
if (isset($_GET['type']) && $_GET['type'] == 'mentor') {
echo "<a href='index.php?'>Go back to all students</a>";
?>
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Full name</th>
        <th>Date of birth</th>
        <th>Username</th>
        <th>Mentor</th>
        <th>Create</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $mstudents = $student->getMentorStudents($_SESSION['user_id']);
    foreach ($mstudents as $mstudent) {
        $mstudent['mentor'] = $user->getMentor($mstudent['user_id']);
        ?>
        <tr>
            <td class="id"><a href="../Students/index.php?id=<?= $mstudent['user_id'] ?>"><i class="fa fa-id-badge"
                                                                                             aria-hidden="true"></i></a>
            </td>
            <td class="full_name"><?= $mstudent['first_name'] . " " . $mstudent['last_name'] ?></td>
            <td class="date_of_birth"><?= $mstudent['date_of_birth'] ?></td>
            <td class="username"><?= $mstudent['username'] ?></td>
            <td class="username"><?= $mstudent['mentor'] ?></td>
            <td class="id"><a href="../Teachers/create.php?student_id=<?= $mstudent['user_id']?>">Create note</a></td>
        </tr>
        <?php
             }
           }
         }
       }
   ?>
</tbody>

</body>
