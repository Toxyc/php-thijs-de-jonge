<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Teachers</title>
    <link rel="stylesheet" href="../../include/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
<?php

require_once('../../include/autoload.php');
require_once('../../include/session.php');
require_once('../../include/common.php');


$pdo = new MyPDO();
$note = new Notes($pdo);
$user = new Users($pdo);
$user->checkForLoginPost();
$errors = [];

?>

<form class="form" action="index.php" method="POST">
  <?php $loggedIn = $user->checkForSession() ?>
</form>

<?php
if ($loggedIn) { $rank = $user->checkRank(); }
if (isset($rank)) {
  if ($rank == 'Admin' || $rank == 'Teacher') { $students = $user->getStudents(); }
}
else {
  $msg = "You have to be logged in as an admin or teacher to view this page.";
}
if (isset($_GET['student_id'])) {
?>
<h1>Create note</h1>
        <form action="create.php" method="POST">
            <label for="student_id">
                <input type="hidden" name="student_id" value="<?= $_GET['student_id'] ?>">
            </label><br>
            <label for="id">Student's name
                <input type="text" value="<?= $note->getStudentFullName($_GET['student_id']); ?>" disabled>
            </label><br>
            <label for="title">Title
                <input type="text" name="title">
            </label><br>
            <label for="description">Description<br>
                <textarea style="width:500px; height: 200px;" name="description"></textarea>
            </label><br>
            <label for="permissions"><b>Permissions</b><br>
                <input type="checkbox" name="visible_to_student">Visible to student<br>
                <input type="checkbox" name="visible_to_teachers">Visible to teachers
            </label><br>
            <button type="submit" name="submit">Submit</button>
        </form>

<?php
    $student_id = $_GET['student_id'];
}
    if (isset($_POST['submit'])) {
        $note->insert();
        $user->redirect(0, "../Students/index.php?student_id=$student_id");
}
else {
    $msg = "Invalid request: Must be POST with a student ID";
}
