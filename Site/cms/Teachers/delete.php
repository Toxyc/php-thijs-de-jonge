<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Teachers</title>
    <link rel="stylesheet" href="../../include/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
<?php

require_once('../../include/autoload.php');
require_once('../../include/session.php');
require_once('../../include/common.php');


$pdo = new MyPDO();
$user = new Users($pdo);
$note = new Notes($pdo);
$user->checkForLoginPost();
$noteByID = $note->getNoteByID($_GET['id']);

?>

<form class="form" action="index.php" method="POST">
  <?php $loggedIn = $user->checkForSession(); ?>
</form>

<?php
if ($loggedIn) { $rank = $user->checkRank(); }
if (isset($rank)) {
  if ($rank == 'Admin' || ($rank == 'Teacher' && $noteByID['author_id'] == $_SESSION['user_id'])) {
      $note->delete();
      $user->redirect(0, '../Teachers/index.php');
  }
}
else {
  $msg = "You have to be logged in as an admin or teacher to view this page.";
}
if (isset($_GET['student_id'])) {
    $student_id = $_GET['student_id'];
    $user->del($student_id);
}
