<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Notes</title>
    <link rel="stylesheet" href="../../include/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>

<?php

require_once('../../include/autoload.php');
require_once('../../include/session.php');
require_once('../../include/common.php');

$pdo = new MyPDO();
$user = new Users($pdo);
$note = new Notes($pdo);

$user->checkForLoginPost();
?>

<form class="form" action="index.php" method="POST">
  <?php $loggedIn = $user->checkForSession() ?>
</form>

<?php
  if ($loggedIn) { $rank = $user->checkRank(); }
  if (isset($rank)) {
    if ($rank == 'Admin' || $rank == 'Teacher') { echo "You are currently on the students index page, you can view notes here. <a href='../Teachers/index.php' class='btn_back'>Go back</a>"; }
      if (isset($_GET['id'])){ $notes = $note->getNotesFromStudent($_GET['id']); }
        else { $notes = $note->getNotesFromStudent(); } }
if (isset($notes)) {
  if (empty($notes)) { $msg = "There are no notes available for you at this moment."; }
?>
<table>
  <thead>
    <tr>
      <th>Title</th>
      <th>Description</th>
      <th>Full name</th>
      <th>Created on</th>
      <th>Mentor</th>
      <th>Last edited</th>
      <?php if ($rank == 'Admin' || $rank == 'Teacher') { ?>
      <th>Edit</th>
      <th>Delete</th>
      <?php } ?>
    </tr>
  </thead>

  <tbody>
<?php
  foreach($notes as $note) {
    $fullname = $user->getFullNameFromId($note['student_id']);
    $note['full_name'] = $user->getFullNameFromId($note['student_id']);
    $note['full_name'] = $fullname['first_name'] ." ". $fullname['last_name'];
    $note['mentor'] = $user->getMentor($note['student_id']);?>
    <tr>
      <td class="title"><?= $note['title'] ?></td>
      <td class="description"><?= $note['description'] ?></td>
      <td class="full_name"><?= $note['full_name'] ?></td>
      <td class="timestamp"><?= $note['creation_date'] ?></td>
      <td class="username"><?= $note['mentor'] ?></td>
      <td class="last_edited"><?= $note['last_edited'] ?></td>
      <?php if ($rank == 'Admin' || $rank == 'Teacher') { ?>
      <td><a href="../Teachers/edit.php?id=<?= $note['note_id'] ?>"><i class="fa fa-wrench" aria-hidden="true"></i></a></td>
      <td><a href="../Teachers/delete.php?id=<?= $note['note_id'] ?>"><i class="fa fa-times" aria-hidden="true"></i></a></td>
      <?php } ?>
    </tr>
    <?php }
} else {
  echo "Please login to view notes.";
}
    ?>
  </tbody>
</table>
<div class="msg">
  <?php  if (isset($msg)) { echo $msg; } ?>
</div>

</body>
